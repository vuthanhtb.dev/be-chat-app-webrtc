package server

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

var AllRooms RoomMap

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type broadcastMsg struct {
	Message map[string]interface{}
	RoomID  string
	Client  *websocket.Conn
}

var broadcast = make(chan broadcastMsg)

func broadcaster() {
	for {
		msg := <-broadcast

		for _, client := range AllRooms.Map[msg.RoomID] {
			if client.Conn != msg.Client {
				if err := client.Conn.WriteJSON(msg.Message); err != nil {
					log.Fatalln(err)
					client.Conn.Close()
				}
			}
		}
	}
}

// CreateRoomRequestHandler Create a room & return roomId
func CreateRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	roomId := AllRooms.CreateRoom()

	type resp struct {
		RoomID string `json:"room_id"`
	}

	log.Println(roomId)
	json.NewEncoder(w).Encode(resp{RoomID: roomId})
}

// JoinRoomRequestHandler will join the client in a particular room
func JoinRoomRequestHandler(w http.ResponseWriter, r *http.Request) {
	roomId, ok := r.URL.Query()["room_id"]

	if !ok {
		log.Println("roomId missing in URL parameters")
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatalln("web socket upgrade error", err)
	}

	AllRooms.InsertIntoRoom(roomId[0], false, ws)

	go broadcaster()

	for {
		var msg broadcastMsg

		err := ws.ReadJSON(&msg.Message)

		if err != nil {
			log.Fatalln(err)
		}

		msg.Client = ws
		msg.RoomID = roomId[0]

		broadcast <- msg
	}
}
